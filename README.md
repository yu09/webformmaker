## Online web form making ever ##
### We make forms look easy ###
Tired of searching web form making? Our webforms is completely free to be tested, used, or contributed to your any business websites

**Our features:**

* Form building
* Optimization
* A/B testing
* Multiple language
* Custom templates
* Validation rules
* Reports

### We offer guaranteed web forms always for your need ###
Our [web form maker](https://formtitan.com) can fine tune every part of your form from general settings down to individual fields, submit actions and emails

Happy web form making!